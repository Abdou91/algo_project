function maxDifference(array_elements) {
    max = 0
    old = 0
    for (var i = 0; i < array_elements.length - 1; i++) {
        max_element = Math.abs(array_elements[i+1] - array_elements[i])
        if (max_element > old) {
            max = max_element
        }
        else {
            max = old
        }
        old = Math.abs(array_elements[i+1] - array_elements[i])
    }
    console.log(max)
    return max
}
module.exports = maxDifference