function deleteKey(object,key) {
    if(!object) return false
    
    for(i=0 ; i<Object.keys(object).length ; i++){
        if(Object.keys(object)[i] === key){
            delete object[Object.keys(object)[i]]
        }
    }
    return object
}

module.exports = deleteKey