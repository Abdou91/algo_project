class Vehicule {
    constructor (model, price) {
        this.model = model
        this.price = price
    }
}

class Car extends Vehicule {

    speedUp(){
        return "La voiture accélére"
    }
    
    start(){
        return "La voiture démarre"
    }
}

class Truck extends Vehicule{

    speedUp(){
        return "Le camion accélére"
    }
    
    start(){
        return "Le camion démarre"
    }
}
const car =new Car('Tesla', 60000)
const truck = new Truck('Man', 100000)

module.exports = { Truck, Car, Vehicule }