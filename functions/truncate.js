String.prototype.truncate = function(pos) {
    let word = this + ""
    if(!word) return false
    if(word.length <= pos) return word

    if(word[pos-1] === " " || word[pos-1] === ","){
        return word.substring(0,pos-1) + "..."
    }

    return word.substring(0,pos) + "..."

    

}

module.exports = String.prototype.truncate