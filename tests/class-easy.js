const { assert, expect } = require('chai');
const { describe, it } = require('mocha');

const User = require('./../functions/class-easy')

module.exports = describe('#1 - Créer une classe utilisateur avec deux attributs (firstName, lastName) et une méthode (spellName)', function() {
  it('should be named correctly', function() {
    assert.equal((new User).constructor.name, 'User')
  });

  it('should have two attributs (firstName, lastName)', function() {
    expect(Object.getOwnPropertyNames(new User)).to.eqls([ 'firstName', 'lastName' ])
  });

  it('should have a method spellName', function() {
    assert.equal(new User('Tcharlyson', 'Platel').spellName(), 'My name is Tcharlyson Platel')
  });
});