#EXERCICES JAVASCRIPT

####Objectif

Basé sur de nombreux tests unitaires, ce repository, une fois cloné, vous permettra de travailler vos notions d'algorithmie.



#### Mise en place

**#1 :** ```git clone <repository>``` | *Pour cloner le projet sur mon ordinateur*

**#2 :** ```cd <mondossier>``` |*Pour naviguer dans le dossier de travail*



*Ouvrez ensuite le projet dans votre éditeur de code (IDE)*



**#3 :** ```npm install``` | *Pour télécharger les node_modules*

**#4 :** ```sudo node_modules/mocha/bin/mocha index.js``` | *Pour lancer les tests*